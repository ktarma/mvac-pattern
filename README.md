# Android Model-View-Activity-Controller pattern #

This is my own perspective of a "pattern" that could be used when building Android applications.  
The difference compared to other well-known patterns is that the Activity and the Controller are separated, so the UI and the program logic can be isolated.

![mvac_eng.png](https://bitbucket.org/repo/5Mrp5r/images/288584432-mvac_eng.png)


### The good ###
1. Controller can be used to listen for application events (like orientation change etc.)
2. Controller can override any (not private) Activity method
3. There can be one Activity (with UI-logic) for multiple Controllers (with program logic)


### The bad ###
1. Although the UI and program logic are isolated, the Activity and the Controller are still connected
2. Usage of dependency injection frameworks (like Android Annotations) is more complicated (but not impossible)