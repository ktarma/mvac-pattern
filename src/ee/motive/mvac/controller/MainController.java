
package ee.motive.mvac.controller;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

import ee.motive.mvac.activity.MainActivity;
import ee.motive.mvac.model.MainModel;

/**
 * This is a Controller of the MVAC-pattern. All the program logic is here.
 */
public class MainController extends MainActivity {

    private int buttonPresses = 0;
    private MainModel mainModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mainModel = new MainModel();

        // Add listener to a button in Activity.
        countButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonPresses++;
                // Update UI through Activity.
                setButtonPresses(buttonPresses);
            }
        });
    }

    // Method with logic, it overrides the method in Activity.
    @Override
    protected int getSomeInfo() {
        int newValue = mainModel.getValue() * 2;
        mainModel.setValue(newValue);
        return newValue;
    }

}
