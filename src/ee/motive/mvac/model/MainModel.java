
package ee.motive.mvac.model;

/**
 * This is a Model of the MVAC-pattern.
 */
public class MainModel {

    private int number;

    /** Constructor. */
    public MainModel() {
        initialize();
    }

    /** Initializing model and its data. */
    private void initialize() {
        number = 1 + 2 + 3;
    }

    public void setValue(int number) {
        this.number = number;
    }

    public int getValue() {
        return number;
    }
}
