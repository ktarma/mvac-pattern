
package ee.motive.mvac.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import ee.motive.mvac.R;

/**
 * This is an Activity of the MVAC-pattern. All the UI logic is here.
 */
public class MainActivity extends Activity {

    protected Button countButton;
    private Button infoButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Controller listens for button presses and updates the view.
        countButton = (Button) findViewById(R.id.countButton);
        countButton.setText("Press me!");

        // Get info from the Controller.
        infoButton = (Button) findViewById(R.id.infoButton);
        infoButton.setText("Get some info!");
        infoButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                infoButton.setText("Got info: " + getSomeInfo());
            }
        });
    }

    // Method to update the UI from the Controller.
    protected void setButtonPresses(int buttonPresses) {
        countButton.setText("Current presses: " + buttonPresses);
    }

    // This method is overridden in the Controller.
    protected int getSomeInfo() {
        return 0;
    }
}
